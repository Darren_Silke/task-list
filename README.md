# Task List

Task List is a simple front-end only Web application to create various tasks to be completed. It was built using HTML, CSS and JavaScript. The [jQuery](https://jquery.com) JavaScript library was also used.

A live version of Task List can be viewed [here](https://thetasklist.netlify.app).

## Running Locally

Clone the repository and open the 'index.html' file in your Web browser.

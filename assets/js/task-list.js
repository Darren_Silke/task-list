// Check off specific task by clicking on it
$("ul").on("click", "li", function() {
	$(this).toggleClass("completed");
});

// Click on trash can icon to delete task
$("ul").on("click", "span", function(event) {
	$(this).parent().fadeOut(500, function() {
		$(this).remove();
	});
	event.stopPropagation();
});

// Add new task to list by pressing enter key
$("input[type='text']").keypress(function(event) {
	if (event.which === 13) {
		// Get new task text from input
		var taskText = $(this).val();
		$(this).val("");
		// Create new li and add to ul
		$("ul").append("<li><span><i class='fas fa-trash-alt'></i></span> " + taskText + "</li>");
	}
});

// Hide/show text input field for adding new task
$(".fa-plus").click(function() {
	$("input[type='text']").fadeToggle();
});
